import { Component } from '@angular/core';
import { Todo } from './models';
import { TodoListService } from './todo-list.service';
// git
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  todoList: Todo[] = [];

  constructor(private todoListService: TodoListService) {
    this.todoListService.todoList.subscribe(
      (todoList) => (this.todoList = todoList)
    );
  }
}
