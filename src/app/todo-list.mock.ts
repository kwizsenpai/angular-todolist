import { Todo } from './models';

export const TodoList: Todo[] = [
  {
    id: 1,
    text: 'Course à LIDL',
    finished: false,
  },
  {
    id: 2,
    text: 'Promener le chien',
    finished: false,
  },
  {
    id: 3,
    text: 'Jouer au ping-pong',
    finished: false,
  },
];
