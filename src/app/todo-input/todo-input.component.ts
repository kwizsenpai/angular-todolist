import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Todo } from '../models';
import { TodoListService } from '../todo-list.service';
import { Store } from '@ngxs/store';
import { Td } from '../state/todolist.actions';

@Component({
  selector: 'app-todo-input',
  templateUrl: './todo-input.component.html',
  styleUrls: ['./todo-input.component.scss'],
})
export class TodoInputComponent {
  constructor(
    private todoListService: TodoListService,
    private readonly store: Store
  ) {}
  addTodo(todoContent: string): any {
    // this.todoListService.addTodo(todoContent);
    this.store.dispatch(new Td.AddItem(todoContent));
  }
}
