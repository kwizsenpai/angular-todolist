import { Component, EventEmitter, Output } from '@angular/core';
import { Filter } from '../models';

@Component({
  selector: 'app-todo-list-filter',
  templateUrl: './todo-list-filter.component.html',
  styleUrls: ['./todo-list-filter.component.scss'],
})
export class TodoListFilterComponent {
  @Output() readonly filterTodos = new EventEmitter<Filter>();
}
