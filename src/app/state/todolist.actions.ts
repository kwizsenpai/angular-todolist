import { Todo } from '../models';

export namespace Td {
  export class LoadList {
    static readonly type = '[TodoList] Load Todo items';

    constructor(public todoList: Todo[]) {}
  }

  export class AddItem {
    static readonly type = '[TodoList] Add Todo item';

    constructor(public todoText: string) {}
  }

  export class DeleteItem {
    static readonly type = '[TodoList] Delete Todo item';

    constructor(public todoId: number) {}
  }

  export class EditList {
    static readonly type = '[TodoList] Edit Todo items';

    constructor(public todoId: number, public newValue: string) {}
  }

  export class FilterList {
    static readonly type = '[TodoList] Filter Todo items';

    constructor(public todoId: number) {}
  }
}
