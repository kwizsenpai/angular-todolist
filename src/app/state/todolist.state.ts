import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { Todo } from '../models';
import { Td } from './todolist.actions';

export interface TodoListStateModel {
  todoList: Todo[];
}

@State<TodoListStateModel>({
  name: 'todoListState',
  defaults: {
    todoList: [],
  },
})
@Injectable()
export class TodoListState {
  @Selector()
  static getTodoList(state: TodoListStateModel) {
    return state.todoList;
  }

  @Action(Td.LoadList)
  loadList(ctx: StateContext<TodoListStateModel>, { todoList }: Td.LoadList) {
    const state = ctx.getState();
    ctx.setState({ todoList });
  }

  @Action(Td.AddItem)
  addTodoItem(ctx: StateContext<TodoListStateModel>, { todoText }: Td.AddItem) {
    const state = ctx.getState();
    let id: number;

    if (state.todoList.length > 0) {
      id = state.todoList[state.todoList.length - 1].id + 1;
    } else {
      id = 1;
    }
    const newTodo: Todo = { id: id, text: todoText, finished: false };

    ctx.patchState({ todoList: [...state.todoList, newTodo] });
  }

  @Action(Td.DeleteItem)
  deleteTodoItem(
    ctx: StateContext<TodoListStateModel>,
    { todoId }: Td.DeleteItem
  ) {
    const state = ctx.getState();
    ctx.patchState({
      todoList: state.todoList.filter((td) => td.id != todoId),
    });
  }

  @Action(Td.EditList)
  editTodoItem(
    ctx: StateContext<TodoListStateModel>,
    { todoId, newValue }: Td.EditList
  ) {
    const state = ctx.getState();
    const todoToEdit = state.todoList.find((todo) => todoId === todo.id);

    if (todoToEdit) {
      todoToEdit.text = newValue;
      ctx.patchState({
        todoList: [
          ...state.todoList.filter((i) => i.id !== todoToEdit.id),
          todoToEdit,
        ].sort((a, b) => {
          return a.id - b.id;
        }),
      });
    }
  }

  @Action(Td.FilterList)
  filterTodoItem(
    ctx: StateContext<TodoListStateModel>,
    { todoId }: Td.FilterList
  ) {
    const state = ctx.getState();
    const todoToFilter = state.todoList.find((todo) => todoId === todo.id);

    if (todoToFilter) {
      todoToFilter.finished = true;
      ctx.patchState({
        todoList: [
          ...state.todoList.filter((i) => i.id !== todoToFilter.id),
          todoToFilter,
        ].sort((a, b) => {
          return a.id - b.id;
        }),
      });
    }
  }
}
