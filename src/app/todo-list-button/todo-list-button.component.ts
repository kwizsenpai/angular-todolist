import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Todo } from '../models';
import { TodoListService } from '../todo-list.service';
import { Store } from '@ngxs/store';
import { Td } from '../state/todolist.actions';

@Component({
  selector: 'app-todo-list-button',
  templateUrl: './todo-list-button.component.html',
  styleUrls: ['./todo-list-button.component.scss'],
})
export class TodoListButtonComponent {
  @Input() todoItem!: Todo;

  constructor(private todoListService: TodoListService, private store: Store) {}

  deleteTodo(id: number): any {
    // this.todoListService.deleteTodo(id);
    this.store.dispatch(new Td.DeleteItem(id));
  }

  filterTodo(id: number): any {
    // this.todoListService.editTodo({ id });
    this.store.dispatch(new Td.FilterList(id));
  }
}
