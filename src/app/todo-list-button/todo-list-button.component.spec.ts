import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TodoListButtonComponent } from './todo-list-button.component';

describe('TodoListButtonComponent', () => {
  let component: TodoListButtonComponent;
  let fixture: ComponentFixture<TodoListButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TodoListButtonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TodoListButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
