export interface Todo {
  id: number;
  text: string;
  finished: boolean;
}

export type Filter = 'all' | 'pending' | 'finished';
