import { Injectable } from '@angular/core';
import { TodoList } from './todo-list.mock';
import { Todo } from './models';
import { BehaviorSubject, Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class TodoListService {
  todoList: BehaviorSubject<Todo[]> = new BehaviorSubject(TodoList);

  constructor() {}

  echoHello(): any {
    return console.log('helloWorld !');
  }

  addTodo(todoContent: string) {
    const todo: Todo = {
      id: this.getLastId(this.todoList.getValue()),
      text: todoContent,
      finished: false,
    };
    // this.todoList.next([...this.todoList.getValue(), todo]);
    this.editTodolist(todo);
  }

  getLastId(todoList: Todo[]): number {
    if (todoList.length > 0) {
      return todoList[todoList.length - 1].id + 1;
    } else {
      return 1;
    }
  }

  deleteTodo(id: number) {
    this.todoList.next(
      this.todoList.getValue().filter((todo) => id !== todo.id)
    );
  }

  findTodoById(id: number): Todo {
    const todoById = this.todoList.getValue().find((todo) => id === todo.id);
    return todoById!;
  }

  editTodolist(addedTodo: Todo) {
    this.todoList.next(
      [...this.todoList.getValue(), addedTodo].sort((a, b) => {
        return a.id - b.id;
      })
    );
  }
  editTodo({ id, newValue }: { id: number; newValue?: string }) {
    const todoToEdit = this.findTodoById(id);
    if (todoToEdit) {
      this.deleteTodo(id);
      if (newValue) {
        todoToEdit.text = newValue;
      } else {
        todoToEdit.finished = true;
      }
      this.editTodolist(todoToEdit);
    }
  }
}
