import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Todo, Filter } from '../models';
import { Td } from '../state/todolist.actions';
import { TodoListState } from '../state/todolist.state';
import { TodoList } from '../todo-list.mock';
import { TodoListService } from '../todo-list.service';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss'],
})
export class TodoListComponent implements OnInit {
  filter: Filter = 'all';
  editMode!: number;
  // todoList: Observable<Todo[]>;
  todoList$: Observable<Todo[]>;

  constructor(
    public todoListService: TodoListService,
    private readonly store: Store
  ) {
    // this.todoList = this.todoListService.todoList;
    this.todoList$ = this.store.select(TodoListState.getTodoList);
  }

  echoHello(): any {
    this.todoListService.echoHello();
  }

  editTodo(id: number, newValue: string): any {
    // this.todoListService.editTodo({ id, newValue });
    this.store.dispatch(new Td.EditList(id, newValue));
    this.editMode = -1;
  }

  filterTodos(todoState: Filter): any {
    this.filter = todoState;
  }

  ngOnInit(): void {
    this.store.dispatch(new Td.LoadList(TodoList));
    this.store.dispatch(new Td.AddItem('yo'));
    this.store.dispatch(new Td.AddItem('chocolate'));
    this.store.dispatch(new Td.DeleteItem(4));
  }
}
